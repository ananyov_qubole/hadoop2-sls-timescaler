import java.io.*;
class JobEditor{
    public static void main()throws IOException{
        BufferedReader rao = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the input file path:");
        /** /usr/lib/hadoop2/share/hadoop/tools/sls/sample-data/2jobs2min-rumen-jh.json */
        String infilepath = rao.readLine();
        BufferedReader filehandle = new BufferedReader(new FileReader(infilepath));
        
        System.out.println("Enter the output file path:");
        /** /usr/lib/hadoop2/share/hadoop/tools/sls/sample-data/scaledJob.json */
        String outfilepath = rao.readLine();
        File outfile = new File(outfilepath);
        outfile.createNewFile();
        FileWriter fw = new FileWriter(outfile);
              
        System.out.println("Enter the job start time:");
        long starttime = Integer.parseInt(rao.readLine());
        
        System.out.println("Enter the job time scaling factor:");
        long scalefactor = Integer.parseInt(rao.readLine());
        
        String parser;
        long basetime=0;
        //pass-1 to get base time
        while ((parser=filehandle.readLine()) != null){
            if (parser.contains("submitTime")){
                int beginStr = parser.indexOf(":");
                String time = parser.substring(beginStr + 2, parser.length() - 1);
                basetime = basetime==0 || Long.parseLong(time)<basetime ? Long.parseLong(time) : basetime;                
            }
        }
        System.out.println("basetime set = " + basetime);
        
        //pass-2 to scale all the times
        filehandle = new BufferedReader(new FileReader(infilepath));
        while ((parser=filehandle.readLine()) != null){
            if (parser.contains("startTime") || parser.contains("finishTime") || 
            parser.contains("submitTime") || parser.contains("launchTime")){
                int beginStr = parser.indexOf(":");
                String time = parser.substring(beginStr + 2, parser.length() - 1);
                long rescaletime = (Long.parseLong(time) - basetime + starttime) / scalefactor;
                String modStr = parser.substring(0,beginStr + 2) + rescaletime + ",\n";
                System.out.print(modStr);    
                fw.write(modStr);
            }
            else fw.write(parser + "\n");                
        }
        fw.close();
        filehandle.close();
    }
}